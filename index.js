const PImage = require("pureimage");
const fs = require("fs");
const R = require("ramda");
const cliProgress = require('cli-progress');
const {randomInt} = require("./random.js");
const {getRandomColor} = require("./colors.js");

// constants
const MM_PER_INCH = 25.4;

// paper dimensions (mm)
const A4 = Object.freeze({width: 210, height: 297});
const A3 = Object.freeze({width: 297, height: 420});
const A2 = Object.freeze({width: 420, height: 594});
const A1 = Object.freeze({width: 594, height: 841});
const A0 = Object.freeze({width: 841, height: 1189});

const DPI = 300;
const SPOT_RADIUS = 2.5; // in mm

const bar = new cliProgress.SingleBar({}, cliProgress.Presets.shades_classic);

function renderImage(paperDimensions, dpi, spotRadius, numSpots, colorHue) {
    console.log(`Rendering image ${paperDimensions.height} cm x ${paperDimensions.width} cm (${dpi} DPI) - ${colorHue} - ${numSpots} Dots (Radius: ${spotRadius} cm).`)
    bar.start(numSpots, 0);
    const {width, height} = calcPixelDimensions(paperDimensions, dpi);
    const spotRadiusPx = calcPixel(spotRadius, dpi);

    const img = PImage.make(width, height, {})
    const ctx = img.getContext('2d');

    // background
    ctx.fillStyle = getRandomColor(colorHue);
    ctx.fillRect(0, 0, width, height);

    // sports
    R.times((n) => {
        if ((n + 1) % 100 === 0) {
            bar.update(n + 1);
        }

        const color = getRandomColor(colorHue);
        const xPos = randomInt(0, width);
        const yPos = randomInt(0, height);

        // create circle
        ctx.beginPath();
        ctx.arc(xPos, yPos, spotRadiusPx, 0, 2 * Math.PI, false); // Outer circle
        ctx.fillStyle = color;
        ctx.fill();
    }, numSpots);

    bar.stop();
    return img;
}

function updateProgressBar() {
}

async function saveImage(img, path) {
    return PImage.encodePNGToStream(img, fs.createWriteStream(path));
}


/**
 * @param width in mm
 * @param height in mm
 * @param dpi
 * @return {{width: number, height: number}} in px
 */
function calcPixelDimensions({width, height}, dpi) {
    const pixelWidth = calcPixel(width, dpi);
    const pixelHeight = calcPixel(height, dpi);

    return {width: pixelWidth, height: pixelHeight};
}

function calcPixel(length, dpi) {
    return Math.floor(dpi * length / MM_PER_INCH);
}


(async () => {
    console.log("Start generating images");

    // Green
    //const a4GreenImg = renderImage(A4, DPI, SPOT_RADIUS, 10000, "green");
    //await saveImage(a4GreenImg, "A4_green.png");

    //const a3GreenImg = renderImage(A3, DPI, SPOT_RADIUS, 30000, "green");
    //await saveImage(a3GreenImg, "A3_green.png");

    //const a2GreenImg = renderImage(A2, DPI, SPOT_RADIUS, 50000, "green");
    //await saveImage(a2GreenImg, "A2_green.png");

    const a1GreenImg = renderImage(A1, DPI, SPOT_RADIUS, 100000, "green");
    await saveImage(a1GreenImg, "A1_green.png");

    // Brown
    //const a4BrownImg = renderImage(A4, DPI, SPOT_RADIUS, 10000, "brown");
    //await saveImage(a4BrownImg, "A4_brown.png");

    //const a3BrownImg = renderImage(A3, DPI, SPOT_RADIUS, 30000, "brown");
    //await saveImage(a3BrownImg, "A3_brown.png");

    //const a2BrownImg = renderImage(A2, DPI, SPOT_RADIUS, 50000, "brown");
    //await saveImage(a2BrownImg, "A2_brown.png");

    const a1BrownImg = renderImage(A1, DPI, SPOT_RADIUS, 100000, "brown");
    await saveImage(a1BrownImg, "A1_brown.png");

    console.log("Done!")
})();
