const {randomInt} = require("./random.js");

function getRandomColor(hue) {
    switch (hue) {
        case "green":
            return GREEN_COLORS[randomInt(0, GREEN_COLORS.length - 1)];
        case "brown":
            return BROWN_COLORS[randomInt(0, BROWN_COLORS.length - 1)];
        default:
            throw new Error(`Hue ${hue} not supported yet.`);
    }
}

const GREEN_COLORS = [
    "#226117",
    "#238511",
    "#15380c",
    "#0d4702",
    "#1d3618",
    "#213d1e",
    "#274f1f",
    "#1d6915",
    "#148c2d",
    "#5a8210",
    "#445c17",
    "#6b8637",
    "#d3cb1b",
    "#969a14",
    "#5b9300",
    "#c4c104",
];

const BROWN_COLORS = [
    "#211c16",
    "#38342f",
    "#42403c",
    "#1a1815",
    "#362c1f",
    "#332615",
    "#594325",
    "#855f2e",
    "#70440a",
    "#808080",
    "#626262",
    "#4d4d4d",
];

const BROWN_COLORS_BACK = [
    "#211c16",
    "#38342f",
    "#42403c",
    "#1a1815",
    "#362c1f",
    "#332615",
    "#594325",
    "#855f2e",
    "#70440a",
    "#464d24",
    "#2e301e"
];

module.exports = {
    getRandomColor
}