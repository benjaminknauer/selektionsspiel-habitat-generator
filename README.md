# Habitat-Generator für Selektionsspiele im Biologieunterricht

## Requirements
- node.js & npm

## Install
```sh
npm install
```

## gewünschtes Format und Farben auswählen
`index.js` anpassen

## Habitat generieren
```sh
npm run generate
```
